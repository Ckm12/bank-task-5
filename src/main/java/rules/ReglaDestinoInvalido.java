package rules;

import bank.Transaccion;

public class ReglaDestinoInvalido implements ReglaDeNegocio {

    public String validate(Transaccion transaccion){
        if (transaccion.cuenta.activa != Boolean.TRUE){

            return "Regla violada, Cuenta destino inactiva o invalida";
        }
        return "";

    }
}
