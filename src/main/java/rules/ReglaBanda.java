package rules;

import bank.Transaccion;

public class ReglaBanda implements ReglaDeNegocio{
    public String validate(Transaccion transaccion){
        if (!transaccion.tarjetaChip != Boolean.TRUE){
            return "Regla violada: No es banda";
        }
        return "";
    }

}
