package bank;

import rules.*;
import FactoryMethod.FabricaReglas;
import bank.Transaccion;
import bank.Cuenta;
import bank.Transaccion;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static FactoryMethod.FabricaReglas.generarConfiguracion;

public class Main {
    public static void main(String arg[]){
        System.out.println("SOLID");

        List<ReglaDeNegocio>reglas = new ArrayList<>();
        reglas.add(new ReglaActiva());
        reglas.add(new ReglaChip());
        reglas.add(new ReglaTransaccionMaxima());
        reglas.add(new MontoSoportado());
        reglas.add(new ReglaCero());

        Ejecutor ejecutorConsignacion = new Ejecutor(reglas);




        Cuenta cuentaPersona1 = new Cuenta();
            cuentaPersona1.numeroCuenta="2365225811420325";
            cuentaPersona1.idenficacion="1080041360";
            cuentaPersona1.nombre="Jose Tello";
            cuentaPersona1.monto=100000.00;
            cuentaPersona1.activa=true;
            cuentaPersona1.tarjetaChip=true;



        Transaccion transaccion = new Transaccion();
        transaccion.cuenta= cuentaPersona1;
        transaccion.tipoTransaccion="Consgnación";
        transaccion.fecha= LocalDate.now();
        transaccion.valor= 120000.0;
        transaccion.tarjetaChip=true;

        Ejecutor ejecutorTransferencia = new Ejecutor(generarConfiguracion("TRANSFERENCIA"));
        Ejecutor ejecutorCredito = new Ejecutor(generarConfiguracion("CREDITO"));

        List<String> reglasVioladas = ejecutorCredito.moverDinero(transaccion);
        List<String> reglasVioladasTransferencia = ejecutorTransferencia.moverDinero(transaccion);
        reglasVioladas.forEach(System.out::println);
        reglasVioladasTransferencia.forEach(System.out::println);







    }

}
