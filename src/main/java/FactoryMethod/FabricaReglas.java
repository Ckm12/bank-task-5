package FactoryMethod;
import rules.*;
//import rules.ReglaMontoNegativo;
import rules.ReglaDeNegocio;
import rules.ReglaImpuesto;

import java.util.ArrayList;
import java.util.List;

public class FabricaReglas {
    public static ReglaDeNegocio generateRule(String  ruleName){
        if (ruleName.equals("ACTIVA")){
            return new ReglaActiva();
        }
        if (ruleName.equals("BANDA")){
            return new ReglaBanda();
        }
        if (ruleName.equals("MONTO_CERO")){
            return new MontoSoportado();
        }
        if (ruleName.equals("DESTINO")){
            return new ReglaDestinoInvalido();
        }
        if (ruleName.equals("MONTO_NEGATIVO")){
            return new MontoSoportado();
        }
        if (ruleName.equals("IMPUESTO")){
            return new ReglaImpuesto();
        }
        if (ruleName.equals("TRANSACCION_MAXIMA")){
            return new ReglaTransaccionMaxima();
        }
        if (ruleName.equals("TRANSACCION_MINIMA")){
            return new ReglaTransaccionMinima();
        }

        throw new RuntimeException("Regla no encontrada");
    }
    public static List<ReglaDeNegocio> generarConfiguracion(String configuracion){
        List<ReglaDeNegocio> reglasEfectivo = new ArrayList<>();

        if (configuracion.equals("CREDITO")){
            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_CERO"));
            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_NEGATIVO"));
            reglasEfectivo.add(FabricaReglas.generateRule("TRANSACCION_MINIMA"));
        }
        if (configuracion.equals("TRANSFERENCIA")){
            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_CERO"));
            reglasEfectivo.add(FabricaReglas.generateRule("MONTO_NEGATIVO"));
            reglasEfectivo.add(FabricaReglas.generateRule("TRANSACCION_MINIMA"));
        }
        return reglasEfectivo;
    }
}
